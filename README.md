## Example of CI configuration

Current setup assumes that CD is configured from Azure Devops side and triggered by pushing code to master/dev/stage brunches.

Our goal is to configure Bitbucket Pipeline so it runs linter check and build before merge code to branches that are connected with Azure Devops.

In the root of the project placed [bitbucket-pipelines.yml](https://bitbucket.org/zaporozhetsAnton/ci-test/src/master/bitbucket-pipelines.yml) it is a pipeline configuration file. Currently it invokes `npm run lint` on every commit and `npm run build` for every pull request. But we can be flexible and it is possible [trigger CI](https://support.atlassian.com/bitbucket-cloud/docs/pipeline-triggers) manually, for a specific branch, for PRs, for each commit, and we can combine different triggers.

When developer creates PR it triggers CI and all reviewers can see the status of CI for each PR. For example [here](https://bitbucket.org/zaporozhetsAnton/ci-test/pull-requests) we have two PRs, one that has passed all CI checks and one that has failed. Every time anyone pushes code and CI fails developer immediately receives an email with a message that in his commit CI failed and a link on CI build. Also when assigned reviewers notice that PR that is on review has failed CI status, the reviewer can skip reviewing until CI issues are resolved.

Additionally in [repository’s branch permissions](https://bitbucket.org/zaporozhetsAnton/ci-test/admin/branch-permissions) possible to add settings to inform the developer that tries to merge PR with failed CI build that this PR is invalid, it is not forbidden to merge PR but at least you will be informed.

Benefits from CI: all developers can see CI status for all PRs and it is impossible to merge invalid PR insensibly, when CI is configured it is possible to get rid of husky or other git hooks and all checks will be triggered on the Bitbucket side and everyone will see the result of these checks. Also all linting scripts stay in the project’s package.json and developer if he Is not sure can manually run linter and build scripts before pushing code to bitbucket, or if he is sure that code is good these steps can be skipped and that will be invoked on bitbucket side. Additionally it is also convenient that we can see statuses [near each branch](https://bitbucket.org/zaporozhetsAnton/ci-test/branches/) of the project.

[Pricing](https://www.atlassian.com/software/bitbucket/pricing) in case we need to reduce the cost we can run CI only when we create or update PR.

Take a look at tabs [Branches](https://bitbucket.org/zaporozhetsAnton/ci-test/branches/), [Pull requests](https://bitbucket.org/zaporozhetsAnton/ci-test/pull-requests/) and [Pipelines](https://bitbucket.org/zaporozhetsAnton/ci-test/addon/pipelines/home) there you can see how convenient it is to see CI statuses, it gives you information which branch/PR has which status.
